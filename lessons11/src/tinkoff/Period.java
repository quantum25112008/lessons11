package tinkoff;

public class Period {
    private String validFrom;
    private String validUntil;
    private String repeatability;

    public Period() {
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }

    public String getRepeatability() {
        return repeatability;
    }

    public void setRepeatability(String repeatability) {
        this.repeatability = repeatability;
    }
    @Override
    public String toString() {
        return "period{" +
                "validFrom='" + validFrom + '\'' +
                ", validUntil='" + validUntil + '\'' +
                ", repeatability='" + repeatability + '\'' +
                '}';
    }

}
