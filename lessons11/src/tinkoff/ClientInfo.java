package tinkoff;

public class ClientInfo {
    private String grade;
    boolean isFulfillConditions = true;
    private String counterInfo;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public boolean isFulfillConditions() {
        return isFulfillConditions;
    }

    public void setFulfillConditions(boolean fulfillConditions) {
        isFulfillConditions = fulfillConditions;
    }

    public String getCounterInfo() {
        return counterInfo;
    }

    public void setCounterInfo(String counterInfo) {
        this.counterInfo = counterInfo;
    }
    @Override
    public String toString() {
        return "ClientInfo{" +
                "grade='" + grade + '\'' +
                ", isFulfillConditions=" + isFulfillConditions +
                ", counterInfo='" + counterInfo + '\'' +
                '}';
    }
}
