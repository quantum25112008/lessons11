package tinkoff;

public class CounterInfo {
    private String count;
    boolean isInfinity = true;
    Period period;

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public CounterInfo() {
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean isInfinity() {
        return isInfinity;
    }

    public void setInfinity(boolean infinity) {
        isInfinity = infinity;
    }
    @Override
    public String toString() {
        return "CounterInfo{" +
                "count='" + count + '\'' +
                ", isInfinity=" + isInfinity +
                ", period='" + period + '\''
                ;
    }

}
