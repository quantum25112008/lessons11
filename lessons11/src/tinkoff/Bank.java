package tinkoff;

public class Bank {
    private String bankName;
    private String bankAdress;
    private String corrAccount;
    private String bankInn;
    private String bankBic;

    public Bank(){

    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAdress() {
        return bankAdress;
    }

    public void setBankAdress(String bankAdress) {
        this.bankAdress = bankAdress;
    }

    public String getCorrAccount() {
        return corrAccount;
    }

    public void setCorrAccount(String corrAccount) {
        this.corrAccount = corrAccount;
    }

    public String getBankInn() {
        return bankInn;
    }

    public void setBankInn(String bankInn) {
        this.bankInn = bankInn;
    }

    public String getBankBic() {
        return bankBic;
    }

    public void setBankBic(String bankBic) {
        this.bankBic = bankBic;
    }
    @Override
    public String toString() {
        return "Bank{" +
                "bankName='" + bankName + '\'' +
                ", bankAdress='" + bankAdress + '\'' +
                ", corrAccount='" + corrAccount + '\'' +
                ", bankInn='" + bankInn + '\'' +
                ", bankBic='" + bankBic + '\'' +
                '}';
    }
}
