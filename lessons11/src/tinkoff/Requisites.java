package tinkoff;

public class Requisites {
    private String fullname;
    private String adress;
    private String inn;
    private String kpp;
    private String ogrn;

    public Requisites() {
    }


    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getOgrn() {
        return ogrn;
    }

    public void setOgrn(String ogrn) {
        this.ogrn = ogrn;
    }

    @Override
    public String toString() {
        return
                "fullname : '" + fullname + '\'' +
                ", adress : '" + adress + '\'' +
                ", inn : '" + inn + '\'' +
                ", kpp : '" + kpp + '\'' +
                ", ogrn : '" + ogrn + '\''
                ;
    }


}
