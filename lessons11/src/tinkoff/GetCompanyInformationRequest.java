package tinkoff;

public class GetCompanyInformationRequest {

    private String name;
    private String city;
    private Requisites requisites;
    private String registrationDate;
    private Bank bank;
    private String taxationScheme;

    public GetCompanyInformationRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Requisites getRequisites() {
        return requisites;
    }

    public void setRequisites(Requisites requisites) {
        this.requisites = requisites;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getTaxationScheme() {
        return taxationScheme;
    }

    public void setTaxationScheme(String taxationScheme) {
        this.taxationScheme = taxationScheme;
    }
    @Override
    public String toString() {
        return "{" +
                "name :'" + name + '\'' +
                ", city :'" + city + '\'' +
                ", requisites :" + requisites +
                ", registrationDate : {" + registrationDate + '\'' +
                ", bank : {" + bank +
                ", taxationScheme :" + taxationScheme + '\'' +
                '}';
    }
}
