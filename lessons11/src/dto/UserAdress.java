package dto;

public class UserAdress {

    protected String adress;
    protected String city;
    protected String country;
    protected String PassportDate;

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPassportDate() {
        return PassportDate;
    }

    public void setPassportDate(String passportDate) {
        PassportDate = passportDate;
    }
    @Override
    public String toString() {
        return "UserAdress{" +
                "adress='" + adress + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", PassportDate='" + PassportDate + '\'' +
                '}';
    }
}
