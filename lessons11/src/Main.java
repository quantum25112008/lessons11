import dto.UserAdress;
import tinkoff.*;

public class Main {
    public static void main(String[] args) {
//        GetCompanyInformationRequest gcr = new GetCompanyInformationRequest();
//        Requisites requisites = new Requisites();
//        Bank bank = new Bank();
//
//        gcr.setName("ООО \"Рога и Копыта\"");
//        gcr.setCity("Москва");
//        gcr.setRegistrationDate("2019-01-01");
//        gcr.setTaxationScheme("OSNO");
//
//        requisites.setFullname("Общество");
//        requisites.setAdress("Москва");
//        requisites.setInn("123456789");
//        requisites.setKpp("1234567890");
//        requisites.setOgrn("1278412389047");
//
//        bank.setBankName("Воскресенье");
//        bank.setBankAdress("СпБ");
//        bank.setCorrAccount("46129786");
//        bank.setBankInn("1234567890");
//        bank.setBankBic("68590468905486");
//
//        gcr.setRequisites(requisites);
//        gcr.setBank(bank);
//
//        System.out.println(gcr.toString());

        ClientInfo clientInfo = new ClientInfo();
        CounterInfo counterInfo = new CounterInfo();
        Period period = new Period();

        clientInfo.setGrade("NONE");
        clientInfo.setFulfillConditions(true);

        counterInfo.setCount("1");
        counterInfo.setInfinity(true);

        period.setValidFrom("2023-01-15T12:00:00");
        period.setValidUntil("2023-02-15T12:00:00");
        period.setRepeatability("MONTH");

        counterInfo.setPeriod(period);

        System.out.println(clientInfo.toString());
        System.out.println(counterInfo.toString());

    }
}